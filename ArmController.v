`timescale 10ns/1ns

`define SIMULATION
`define CLOCK_PERIOD_NS 20 // Clock of 50 MHz
`define CLOCKS_PER_BIT 434

`ifdef SIMULATION
	`include "uart_tx.v"
	`include "controller.v"
`endif

module ArmController(CLOCK_50, GPIO_0, LEDR, HEX0);
	input CLOCK_50;
	output [35:0] GPIO_0;
	output [5:0] LEDR;
	output [6:0] HEX0;

	wire w_clock;
	wire w_tx_enable;
	wire [7:0] w_tx_byte;
	wire w_tx_done;
	wire w_tx_serial;
	wire [3:0] w_state;

	controller controller(
		.i_clock(w_clock),
		.i_tx_done(w_tx_done),
		.o_tx_enable(w_tx_enable),
		.o_tx_byte(w_tx_byte),
		.o_state(w_state)
	);

	uart_tx uart_tx(
		.i_Clock(w_clock),
		.i_Tx_DV(w_tx_enable),
		.i_Tx_Byte(w_tx_byte),
		.o_Tx_Serial(w_tx_serial),
		.o_Tx_Done(w_tx_done)
	);

`ifdef SIMULATION
	assign w_clock = r_clock;
`else
	assign w_clock = CLOCK_50;
	assign LEDR[0] = w_state[0];
	assign LEDR[1] = w_state[1];
	assign LEDR[2] = w_state[2];
	assign LEDR[3] = w_state[3];
	assign LEDR[5] = w_tx_serial;
	assign GPIO_0[0] = w_tx_serial;
`endif

`ifdef SIMULATION
	reg r_clock;

	initial begin
		$dumpfile("simulation.vcd");
		$dumpvars;

		r_clock <= 0;
		#300000 $finish;
	end

	// always #(c_CLOCK_PERIOD_NS/2) r_clock = ~r_clock;
	always #1 r_clock = ~r_clock;
`endif
endmodule
