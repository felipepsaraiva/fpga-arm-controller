`ifdef SIMULATION
	`define PLAYTIME_DELAY 28'd10
`else
	`define PLAYTIME_DELAY 28'd125000000
`endif

`define NUM_SERVOS 8'd6
`define NUM_POSITIONS 8
`define BROADCAST_ID 8'd254
`define SET_POSITION_CONTROL 8'd0
`define PLAYTIME 8'd250

`define SJOG_CMD 8'h06
`define SJOG_HEADER_SIZE 8'd8
`define SJOG_DATA_SIZE 8'd4
`define SJOG_SIZE 8'd32
`define CMD_MAX_ADDR 31

module controller(
  input i_clock,
  input i_tx_done,
  output o_tx_enable,
  output [7:0] o_tx_byte,
	output [3:0] o_state
);

	parameter s_INITIAL 			= 0;
	parameter s_SELECT_POS 		= 1;
	parameter s_ASSEMBLE_CMD	= 2;
	parameter s_CALCULATE_XOR	= 3;
	parameter s_CHECKSUMS			= 4;
	parameter s_SELECT_BYTE		= 5;
	parameter s_CONFIG_TX			= 6;
	parameter s_WAIT_TX				= 7;
	parameter s_DELAY					= 8;

	reg [27:0] r_clock_count = 0;
	reg [3:0] r_state = 0;
	reg [7:0] r_checksum;

	reg [2:0] r_selected_pos = 0;
	reg [2:0] r_servo_addr = 0;
	reg [4:0] r_byte_addr = 0;

	reg [7:0] r_mem_id [0:`NUM_SERVOS-1];
	reg [15:0] r_mem_pos [0:`NUM_POSITIONS-1][0:`NUM_SERVOS-1];
	reg [7:0] r_mem_cmd [0:`CMD_MAX_ADDR];

	reg r_tx_enable = 0;
	reg [7:0] r_tx_byte;

  assign o_tx_enable = r_tx_enable;
  assign o_tx_byte = r_tx_byte;
	assign o_state = r_state;

	always @(posedge i_clock) begin
		case (r_state)
			s_INITIAL: begin
				r_clock_count <= 0;
				r_tx_enable <= 0;

				r_mem_cmd[0] <= 8'hFF;
				r_mem_cmd[1] <= 8'hFF;
				r_mem_cmd[2] <= `SJOG_SIZE;
				r_mem_cmd[3] <= `BROADCAST_ID;
				r_mem_cmd[4] <= `SJOG_CMD;
				r_mem_cmd[7] <= `PLAYTIME;

				r_mem_id[0] <= 8'd9;
				r_mem_id[1] <= 8'd10;
				r_mem_id[2] <= 8'd7;
				r_mem_id[3] <= 8'd8;
				r_mem_id[4] <= 8'd11;
				r_mem_id[5] <= 8'd6;

				r_mem_pos[0][0] <= 16'd372;
				r_mem_pos[0][1] <= 16'd510;
				r_mem_pos[0][2] <= 16'd387;
				r_mem_pos[0][3] <= 16'd686;
				r_mem_pos[0][4] <= 16'd246;
				r_mem_pos[0][5] <= 16'd386;

				r_mem_pos[1][0] <= 16'd372;
				r_mem_pos[1][1] <= 16'd510;
				r_mem_pos[1][2] <= 16'd387;
				r_mem_pos[1][3] <= 16'd686;
				r_mem_pos[1][4] <= 16'd246;
				r_mem_pos[1][5] <= 16'd461;

				r_mem_pos[2][0] <= 16'd372;
				r_mem_pos[2][1] <= 16'd452;
				r_mem_pos[2][2] <= 16'd474;
				r_mem_pos[2][3] <= 16'd567;
				r_mem_pos[2][4] <= 16'd246;
				r_mem_pos[2][5] <= 16'd461;

				r_mem_pos[3][0] <= 16'd652;
				r_mem_pos[3][1] <= 16'd452;
				r_mem_pos[3][2] <= 16'd474;
				r_mem_pos[3][3] <= 16'd567;
				r_mem_pos[3][4] <= 16'd246;
				r_mem_pos[3][5] <= 16'd461;

				r_mem_pos[4][0] <= 16'd652;
				r_mem_pos[4][1] <= 16'd517;
				r_mem_pos[4][2] <= 16'd380;
				r_mem_pos[4][3] <= 16'd700;
				r_mem_pos[4][4] <= 16'd246;
				r_mem_pos[4][5] <= 16'd461;

				r_mem_pos[5][0] <= 16'd652;
				r_mem_pos[5][1] <= 16'd517;
				r_mem_pos[5][2] <= 16'd380;
				r_mem_pos[5][3] <= 16'd700;
				r_mem_pos[5][4] <= 16'd246;
				r_mem_pos[5][5] <= 16'd386;

				r_mem_pos[6][0] <= 16'd652;
				r_mem_pos[6][1] <= 16'd452;
				r_mem_pos[6][2] <= 16'd474;
				r_mem_pos[6][3] <= 16'd567;
				r_mem_pos[6][4] <= 16'd246;
				r_mem_pos[6][5] <= 16'd386;

				r_mem_pos[7][0] <= 16'd372;
				r_mem_pos[7][1] <= 16'd452;
				r_mem_pos[7][2] <= 16'd474;
				r_mem_pos[7][3] <= 16'd567;
				r_mem_pos[7][4] <= 16'd246;
				r_mem_pos[7][5] <= 16'd386;

				r_selected_pos <= `NUM_POSITIONS-1;
				r_state <= s_SELECT_POS;
			end

			s_SELECT_POS: begin
				if (r_selected_pos >= `NUM_POSITIONS-1) r_selected_pos <= 0;
				else r_selected_pos <= r_selected_pos + 1;

				r_servo_addr <= 0;
				r_byte_addr <= `SJOG_HEADER_SIZE;
				r_state <= s_ASSEMBLE_CMD;
			end

			s_ASSEMBLE_CMD: begin
				r_mem_cmd[r_byte_addr] <= r_mem_pos[r_selected_pos][r_servo_addr][7:0];
				r_mem_cmd[r_byte_addr + 8'd1] <= r_mem_pos[r_selected_pos][r_servo_addr][15:8];
				r_mem_cmd[r_byte_addr + 8'd2] <= `SET_POSITION_CONTROL;
				r_mem_cmd[r_byte_addr + 8'd3] <= r_mem_id[r_servo_addr];

				if (r_servo_addr < `NUM_SERVOS - 1) begin
					r_servo_addr <= r_servo_addr + 1;
					r_byte_addr <= r_byte_addr + 4;
					r_state <= s_ASSEMBLE_CMD;
				end
				else begin
					r_byte_addr <= `SJOG_HEADER_SIZE;
					r_checksum <= (`SJOG_SIZE ^ `BROADCAST_ID ^ `SJOG_CMD ^ `PLAYTIME);
					r_state <= s_CALCULATE_XOR;
				end
			end

			s_CALCULATE_XOR: begin
				r_checksum <= r_checksum ^ r_mem_cmd[r_byte_addr];

				if (r_byte_addr < `SJOG_SIZE - 1) begin
					r_byte_addr <= r_byte_addr + 1;
					r_state <= s_CALCULATE_XOR;
				end
				else begin
					r_byte_addr <= 0;
					r_state <= s_CHECKSUMS;
				end
			end

			s_CHECKSUMS: begin
				r_mem_cmd[5] <= r_checksum & 8'hFE;
				r_mem_cmd[6] <= ~r_checksum & 8'hFE;
				r_byte_addr <= 0;
				r_state <= s_CONFIG_TX;
			end

			s_SELECT_BYTE: begin
				if (r_byte_addr < `SJOG_SIZE - 1) begin
					r_byte_addr <= r_byte_addr + 1;
					r_state <= s_CONFIG_TX;
				end
				else begin
					r_clock_count <= 0;
					r_state <= s_DELAY;
				end
			end

			s_CONFIG_TX: begin
				r_tx_byte <= r_mem_cmd[r_byte_addr];
				r_tx_enable <= 1;
				r_state <= s_WAIT_TX;
			end

			s_WAIT_TX: begin
				r_tx_enable <= 0;
				if (i_tx_done == 1) r_state <= s_SELECT_BYTE;
			end

			s_DELAY: begin
        `ifdef SIMULATION
          if (r_clock_count == 0) DMEM_CMD();
        `endif

				if (r_clock_count < `PLAYTIME_DELAY) begin
					r_clock_count <= r_clock_count + 1;
					r_state <= s_DELAY;
				end
				else begin
					r_clock_count <= 0;
					r_state <= s_SELECT_POS;
				end
			end

			default: r_state <= s_INITIAL;
		endcase
	end

`ifdef SIMULATION
	task DMEM_CMD;
		begin
			$display("CMD Memory Dump:");
			$display(r_mem_cmd[0]);
			$display(r_mem_cmd[1]);
			$display(r_mem_cmd[2]);
			$display(r_mem_cmd[3]);
			$display(r_mem_cmd[4]);
			$display(r_mem_cmd[5]);
			$display(r_mem_cmd[6]);
			$display(r_mem_cmd[7]);
      $display("");
			$display(r_mem_cmd[8]);
			$display(r_mem_cmd[9]);
			$display(r_mem_cmd[10]);
			$display(r_mem_cmd[11]);
      $display("");
			$display(r_mem_cmd[12]);
			$display(r_mem_cmd[13]);
			$display(r_mem_cmd[14]);
			$display(r_mem_cmd[15]);
      $display("");
			$display(r_mem_cmd[16]);
			$display(r_mem_cmd[17]);
			$display(r_mem_cmd[18]);
			$display(r_mem_cmd[19]);
      $display("");
			$display(r_mem_cmd[20]);
			$display(r_mem_cmd[21]);
			$display(r_mem_cmd[22]);
			$display(r_mem_cmd[23]);
      $display("");
			$display(r_mem_cmd[24]);
			$display(r_mem_cmd[25]);
			$display(r_mem_cmd[26]);
			$display(r_mem_cmd[27]);
      $display("");
			$display(r_mem_cmd[28]);
			$display(r_mem_cmd[29]);
			$display(r_mem_cmd[30]);
			$display(r_mem_cmd[31]);
			$display("");
		end
	endtask
`endif
endmodule
