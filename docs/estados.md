# Estados

O sistema é modelado como uma máquina de estados. As transições podem ser simplesmente por terminar as tarefas de um estado ou por um evento específico.

1. `INITIAL`: Inicializa a memória e configura os outros componentes do sistema.
1. `SELECT_POS`: Seleciona qual das posições será comandada.
1. `ASSEMBLE_CMD`: Monta o comando que será enviado em memória.
1. `CHECKSUM_1`: Calcula o checksum 1 do comando montado no estado anterior.
1. `CHECKSUM_2`: Calcula o checksum 2 baseado no checksum 1.
1. `SELECT_BYTE`: Seleciona qual byte do comando será enviado.
1. `CONFIG_TX`: Habilita a transmissão serial.
1. `WAIT_TX`: Espera até que a transmissão serial seja finalizada.
1. `DELAY`: Aplica um delay entre dois comandos respeitando o tempo de execução dos motores.
